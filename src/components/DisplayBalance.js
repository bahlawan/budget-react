import React from "react";
import { Statistic } from "semantic-ui-react";

function DisplayBalance({ color, titel, amount, size = "tiny", textAlign = "left" }) {
  return (
    <Statistic size={size} color={color}>
      <Statistic.Label style={{ textAlign: { textAlign } }}>{titel}</Statistic.Label>
      <Statistic.Value>{isNaN(amount) ? 0 : amount} €</Statistic.Value>
    </Statistic>
  );
}

export default DisplayBalance;
