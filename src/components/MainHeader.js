import React from "react";
import { Header } from "semantic-ui-react";

function MainHeader({ titel, type = "h1" }) {
  return <Header as={type}>{titel}</Header>;
}

export default MainHeader;
