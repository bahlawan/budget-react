import React from "react";
import { Button, Modal } from "semantic-ui-react";
import EntryForm from "./EntryForm";
import { useDispatch } from "react-redux";
import { closeEditModal } from "../actions/modals.actions";
import useEntryDetails from "../hooks/useEntryDetails";

function ModalEdit({ isOpen, description, isExpense, amount, id }) {
  const dispatch = useDispatch();
  const entryUpdate = useEntryDetails(description, amount, isExpense);

  return (
    <Modal open={isOpen}>
      <Modal.Header>Edit entry</Modal.Header>
      <Modal.Content>
        <EntryForm
          description={entryUpdate.description}
          isExpense={entryUpdate.isExpense}
          amount={entryUpdate.amount}
          setDescription={entryUpdate.setDescription}
          setIsExpense={entryUpdate.setIsExpense}
          setAmount={entryUpdate.setAmount}
        />
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={() => dispatch(closeEditModal())}>Close</Button>
        <Button onClick={() => entryUpdate.updateEntry(id)} primary>
          Ok
        </Button>
      </Modal.Actions>
    </Modal>
  );
}

export default ModalEdit;
