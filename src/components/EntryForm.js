import React from "react";
import { Checkbox, Form, Segment } from "semantic-ui-react";

function EntryForm({ description, isExpense, amount, setDescription, setIsExpense, setAmount }) {
  return (
    <Segment>
      <Form.Group>
        <Form.Input
          icon="tags"
          width={12}
          label="Description"
          placeholder="New shinny thing"
          value={description}
          onChange={(event) => setDescription(event.target.value)}
        />
        <Form.Input
          width={4}
          label="Value"
          placeholder="100.00"
          icon="euro"
          iconPosition="left"
          value={amount}
          onChange={(event) => setAmount(event.target.value)}
        />
      </Form.Group>
      <Segment compact>
        <Checkbox
          toggle
          label="is expense"
          checked={isExpense}
          onChange={() => setIsExpense((oldState) => !oldState)}
        />
      </Segment>
    </Segment>
  );
}

export default EntryForm;
