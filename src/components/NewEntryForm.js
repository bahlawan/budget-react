import React from "react";
import ButtonSaveOrCancel from "./ButtonSaveOrCancel";
import { Form } from "semantic-ui-react";
import EntryForm from "./EntryForm";
import useEntryDetails from "../hooks/useEntryDetails";

function NewEntryForm() {
  const { description, setDescription, amount, setAmount, isExpense, setIsExpense, addEntry } = useEntryDetails();
  return (
    <Form unstackable>
      <EntryForm
        description={description}
        isExpense={isExpense}
        amount={amount}
        setDescription={setDescription}
        setIsExpense={setIsExpense}
        setAmount={setAmount}
      />
      <ButtonSaveOrCancel addEntry={addEntry} />
    </Form>
  );
}

export default NewEntryForm;
