import axios from "axios";
import { call, put, takeEvery } from "redux-saga/effects";
import entriesTypes from "../actions/entries.actions";

export function* updateEntrySaga() {
  yield takeEvery(entriesTypes.UPDATE_ENTRY, updateEntryInDb);
}

function* updateEntryInDb({ payload }) {
  const { entry } = payload;
  yield call(updateEntry, entry);
  yield call(updateEntryDetails, entry);
  yield put({ type: entriesTypes.UPDATE_ENTRY_RESULT, payload });
}

async function updateEntry({ id, description }) {
  await axios.put(`http://localhost:3001/entries/${id}`, { id, description });
}

async function updateEntryDetails({ id, amount, isExpense }) {
  await axios.put(`http://localhost:3001/values/${id}`, { id, amount, isExpense });
}
