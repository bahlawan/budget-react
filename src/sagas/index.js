//import * as testSaga from "./testSaga";
import * as entriesSaga from "./entriesSaga";
import * as entriesSagaAdd from "./entriesSagaAdd";
import * as entriesSagaDelete from "./entriesSagaDelete";
import * as entriesSagaUpdate from "./entriesSagaUpdate";

export function initSagas(sagaMiddleware) {
  //Object.values(testSaga).forEach(sagaMiddleware.run.bind(sagaMiddleware));
  Object.values(entriesSaga).forEach(sagaMiddleware.run.bind(sagaMiddleware));
  Object.values(entriesSagaAdd).forEach(sagaMiddleware.run.bind(sagaMiddleware));
  Object.values(entriesSagaDelete).forEach(sagaMiddleware.run.bind(sagaMiddleware));
  Object.values(entriesSagaUpdate).forEach(sagaMiddleware.run.bind(sagaMiddleware));
}
