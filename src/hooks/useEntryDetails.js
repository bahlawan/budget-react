import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { addEntryRedux, updateEntryRedux } from "../actions/entries.actions";
import { v4 as uuidv4 } from "uuid";
import { closeEditModal } from "../actions/modals.actions";

function useEntryDetails(desc = "", amo = "", isExp = true) {
  const [description, setDescription] = useState(desc);
  const [isExpense, setIsExpense] = useState(isExp);
  const [amount, setAmount] = useState(amo);
  const dispatch = useDispatch();

  useEffect(() => {
    setDescription(desc);
    setAmount(amo);
    setIsExpense(isExp);
  }, [desc, amo, isExp]);

  function updateEntry(id) {
    dispatch(
      updateEntryRedux(id, {
        id,
        description,
        amount,
        isExpense,
      })
    );
    dispatch(closeEditModal());
    resetValues();
  }

  function addEntry() {
    dispatch(
      addEntryRedux({
        id: uuidv4(),
        description,
        isExpense,
        amount,
      })
    );
    resetValues();
  }

  function resetValues() {
    setDescription("");
    setAmount("");
    setIsExpense(true);
  }

  return {
    description,
    setDescription,
    amount,
    setAmount,
    isExpense,
    setIsExpense,
    addEntry,
    updateEntry,
  };
}

export default useEntryDetails;
